module gitlab.com/terrabitz/reddit-downloader

go 1.14

require (
	github.com/go-ozzo/ozzo-validation/v4 v4.2.1
	github.com/spf13/cobra v1.0.0
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
