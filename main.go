package main

import (
	"gitlab.com/terrabitz/reddit-downloader/cmd"
)

//nolint:gochecknoglobals // These variables may be dynamically inserted at build-time
var (
	version = ""
	commit  = ""
	date    = ""
	builtBy = ""
)

func main() {
	cmd.Execute(cmd.Info{
		Version: version,
		Commit:  commit,
		Date:    date,
		BuiltBy: builtBy,
	})
}
