package cmd

type RedditSubResponse struct {
	Kind string `json:"kind"`
	Data struct {
		Modhash  string `json:"modhash"`
		Dist     int    `json:"dist"`
		Children []struct {
			Kind string     `json:"kind"`
			Data RedditPost `json:"data"`
		} `json:"children"`
		After  string `json:"after"`
		Before string `json:"before"`
	} `json:"data"`
}

type RedditPost struct {
	Title     string  `json:"title,omitempty"`
	Domain    string  `json:"domain,omitempty"`
	URL       string  `json:"url,omitempty"`
	Permalink string  `json:"permalink,omitempty"`
	ID        string  `json:"id,omitempty"`
	PostText  string  `json:"selftext,omitempty"`
	Created   float32 `json:"created,omitempty"`
	NSFW      bool    `json:"over_18,omitempty"`
}
