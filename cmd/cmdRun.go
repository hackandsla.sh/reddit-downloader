package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"sync"
	"time"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/spf13/cobra"
)

var (
	ErrValidation = errors.New("validation error")
)

const UserAgent = "golang:sh.hackandsla.reddit-downloader:0.0.1 (by /u/terrabitz)"

// Initializes the "run" command and starts our program loop.
func newCmdRun() *cobra.Command {
	config := &RunConfig{}

	cmd := &cobra.Command{
		Use:   "run",
		Short: "Runs the reddit downloader for a specific directory",
		Run: func(cmd *cobra.Command, args []string) {
			if err := config.BindArgs(args); err != nil {
				log.Fatalf("Invalid arguments: %v", err)
			}
			if err := config.Validate(); err != nil {
				log.Fatalf("Invalid values: %v", err)
			}

			for {
				UpdateAllArchives(config)

				time.Sleep(config.WaitTime)
			}
		},
	}

	config.BindFlags(cmd)

	return cmd
}

type RunConfig struct {
	Subreddits []string
	Path       string
	WaitTime   time.Duration
	NSFW       bool
}

// Validate validates our given config.
func (c *RunConfig) Validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.Subreddits, validation.Required),
		validation.Field(&c.Path, validation.Required),
		validation.Field(&c.WaitTime, validation.Required),
	)
}

// BindFlags binds and runtime flags (if any).
func (c *RunConfig) BindFlags(cmd *cobra.Command) {
	cmd.Flags().StringSliceVarP(&c.Subreddits, "subreddits", "r", []string{}, "The subreddits to monitor")
	_ = cmd.MarkFlagRequired("subreddits")
	cmd.Flags().StringVarP(&c.Path, "path", "p", ".", "The path to dump the archive in")
	cmd.Flags().DurationVarP(&c.WaitTime, "wait", "w", time.Minute, "The time to wait between requests")
	cmd.Flags().BoolVarP(&c.NSFW, "nsfw", "n", false, "If specified, will include NSFW content")
}

// BindArgs binds any runtime arguments (if any).
func (c *RunConfig) BindArgs(args []string) error {
	return nil
}

// UpdateAllArchives spins off multiple goroutines to download updates for each
// subreddit in parallel.
func UpdateAllArchives(config *RunConfig) {
	wg := sync.WaitGroup{}
	logLock := sync.Mutex{}

	for _, subreddit := range config.Subreddits {
		wg.Add(1)

		go func(subreddit string) {
			defer wg.Done()

			newCount, err := UpdateArchive(subreddit, config)
			if err != nil {
				log.Printf("%v", err)
				return
			}

			if newCount > 0 {
				logLock.Lock()
				defer logLock.Unlock()

				log.Printf("%s: added %v new posts", subreddit, newCount)
			}
		}(subreddit)
	}

	wg.Wait()
}

// UpdateArchive retrieves the latest posts from the given subreddit and saves
// them to disk if there are any new ones. It returns the number of new posts,
// and any errors, if applicable.
func UpdateArchive(subreddit string, config *RunConfig) (newCount int, e error) {
	location := path.Join(config.Path, subreddit+".json")
	if _, err := os.Stat(location); os.IsNotExist(err) {
		//nolint:gosec // This file isn't security-sensitive
		if err := ioutil.WriteFile(location, []byte("{}"), 0644); err != nil {
			return 0, fmt.Errorf("error while creating %s: %w", location, err)
		}
	}

	existing, _ := ioutil.ReadFile(location)
	archive := map[string]RedditPost{}

	if err := json.Unmarshal(existing, &archive); err != nil {
		return 0, fmt.Errorf("error while unmarshaling JSON: %w", err)
	}

	newPosts, err := GetNewPosts(subreddit)
	if err != nil {
		return 0, err
	}

	newCount = 0

	for _, newPost := range newPosts.Data.Children {
		_, ok := archive[newPost.Data.ID]

		if !ok && (!newPost.Data.NSFW || config.NSFW) {
			newCount++

			archive[newPost.Data.ID] = newPost.Data
		}
	}

	if newCount > 0 {
		newArchive, err := json.MarshalIndent(archive, "", " ")
		if err != nil {
			return 0, fmt.Errorf("error while marshaling JSON: %w", err)
		}

		//nolint:gosec // This file isn't security-sensitive
		if err := ioutil.WriteFile(location, newArchive, 0644); err != nil {
			return 0, fmt.Errorf("error while write to '%s': %w", location, err)
		}
	}

	return newCount, nil
}

// GetNewPosts retrieves the latest reddit post listing from the given subreddit.
func GetNewPosts(subreddit string) (*RedditSubResponse, error) {
	url := fmt.Sprintf("https://www.reddit.com/r/%s/.json?limit=100&sort=new", subreddit)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("User-Agent", UserAgent)

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("could not retrieve reddit posts: %w", err)
	}

	newPosts := &RedditSubResponse{}
	if err := json.Unmarshal(body, newPosts); err != nil {
		return nil, err
	}

	return newPosts, nil
}
