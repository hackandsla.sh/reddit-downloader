package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

type Info struct {
	Version string
	Commit  string
	Date    string
	BuiltBy string
}

// Execute the root command.
func Execute(info Info) {
	var rootCmd = initRootCmd()
	rootCmd.Version = info.Version

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func initRootCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "reddit-downloader",
		Short: "reddit-downloader continually watches Reddit and downloads new content",
		Long:  "reddit-downloader continually watches Reddit and downloads new content",
	}
	addSubcommands(cmd)

	return cmd
}

func addSubcommands(cmd *cobra.Command) {
	cmd.AddCommand(newCmdRun())
}
