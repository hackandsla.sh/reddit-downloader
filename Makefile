remove-merged-branches:
	git fetch --prune
	git branch --merged | egrep -v "(^\*|master)" | xargs git branch -d

build-snapshot:
	docker run --rm --privileged -v $(shell pwd):/go/src/gitlab.com/terrabitz/yellowslip -v /var/run/docker.sock:/var/run/docker.sock -w /go/src/gitlab.com/terrabitz/yellowslip -e GITLAB_TOKEN goreleaser/goreleaser:v0.141.0 release --debug --snapshot --skip-publish --rm-dist

gen:
	go generate ./...

test: gen
	go test ./...

lint:
	golangci-lint run

tidy: 
	./build/tidy.sh

pull-master:
	git checkout master
	git pull

release: pull-master test lint tidy
	standard-version