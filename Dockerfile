FROM golang:1.14-buster as builder

# Create the user and group files to run unprivileged
RUN mkdir /user && \
    echo 'nobody:x:65534:65534:nobody:/:' > /user/passwd && \
    echo 'nobody:65534:' > /user/group
RUN apt-get update && \
    apt-get install -y gcc git ca-certificates tzdata xz-utils jq curl

FROM scratch as final
ENV PATH "/"
LABEL author="Trevor Taubitz"
# Import the timezone files
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
# Import the user and group files
COPY --from=builder /user/group /user/passwd /etc/
# Import the CA certs
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
WORKDIR /
# Run as unprivileged
USER nobody:nobody

ARG BIN
# ENV BIN=$BIN
COPY $BIN "/reddit-downloader"
ENTRYPOINT ["/reddit-downloader"]